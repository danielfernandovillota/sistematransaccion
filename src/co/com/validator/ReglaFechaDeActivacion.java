package co.com.validator;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ReglaFechaDeActivacion implements Regla {
    @Override
    public String execute (Transaccion transaccion){
        SimpleDateFormat formater = new SimpleDateFormat( "yyyy-MM-dd");
        try {
            Date fechaMinima = formater.parse("2022-01-01");
            if (transaccion.cuenta.fechaActivacion.before(fechaMinima)){
                return "La cuenta no supera la fecha minima de activacion";
            }

        } catch (ParseException e) {
           return "La cuenta no supera la fecha minima de activacion";
        }

        return"";
    }
}

