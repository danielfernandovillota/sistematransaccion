package co.com.validator;

public class ReglaSaldoSuficiente implements Regla {
    @Override
    public String execute(Transaccion transaccion) {
        if (transaccion.montoMovimiento > transaccion.cuenta.monto) {
            return "El monto de la transaccion supera su monto actual";
        }

        return "";
    }
}
