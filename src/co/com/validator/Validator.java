package co.com.validator;


import java.util.ArrayList;
import java.util.List;
public class Validator {

    private final List<Regla>rules;

    public Validator(List<Regla> rules){
        this.rules = rules;
        System.out.println("Constructor Validador");
    }

    List<String>validarTrasaccion(Transaccion transaccion){
    List <String> reglasVioladas = new ArrayList<>();
    for (Regla rule :this.rules){
        String response = rule.execute(transaccion);
        if (!response.isEmpty())
        {
            reglasVioladas.add(response);

        }
    }
    return reglasVioladas;
    }


}
