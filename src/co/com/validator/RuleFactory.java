package co.com.validator;

public class RuleFactory
{
    public static Regla generateRule(String ruleName){

        if("Cuenta Activa".equals((ruleName)))
        {
            return new ReglaCuentaActiva();
        }
        if("Saldo Suficiente".equals((ruleName)))
        {
            return new ReglaSaldoSuficiente();
        }
        if("Monto Maximo".equals((ruleName)))
        {
            return new ReglaMontoMaximo();
        }
        if("Fecha Activacion".equals((ruleName)))
        {
            return new ReglaFechaEntre();
        }

        return new ReglaCuentaActiva();
    }
}
