package co.com.validator;

public class ReglaMontoValido implements Regla {
    @Override
    public String execute (Transaccion transaccion){
        if(transaccion.montoMovimiento<=0){
            return "El monto de la transaccion debe ser mayor a cero";
        }
        return"";
    }
}
