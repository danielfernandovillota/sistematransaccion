package co.com.validator;

import com.sun.source.tree.ReturnTree;

public class ReglaCuentaActiva implements Regla {
    @Override
    public String execute (Transaccion transaccion){
        if (!transaccion.cuenta.activa){
           return("La cuenta no esta activa");
        }
        return "";
    }
}
