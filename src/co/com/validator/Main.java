package co.com.validator;



import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class Main {


    public static void main(String[] args) {
        Cuenta miCuenta=new Cuenta();
        miCuenta.nombre="Esteban";
        miCuenta.tipo="Ahorro";
        miCuenta.monto=100.0;

        Transaccion pagoSemestre=new Transaccion();
        pagoSemestre.fecha=new Date();
        pagoSemestre.montoMovimiento=0.0;
        pagoSemestre.historico=new ArrayList<>();
        pagoSemestre.cuenta=miCuenta;

        List<Regla> rules =new ArrayList<>();
        rules.add(new ReglaCuentaActiva());
        rules.add(new ReglaMontoValido());
        rules.add(new ReglaSaldoSuficiente());

        Validator validador =new Validator(rules);
        List<String> reglasVioladas=validador.validarTrasaccion(pagoSemestre);

        System.out.println("CUENTA BANCARIA");
        System.out.println("Usuario: "+miCuenta.nombre+"\nMonto: "+miCuenta.monto);
        System.out.println("Fecha de transaccion: "+pagoSemestre.fecha+" \nMonto a transaccion: "+ pagoSemestre.montoMovimiento);
        System.out.println(reglasVioladas.stream().collect(Collectors.joining("\n")));


    }

    private List<Regla> basicConfig (){

        List<Regla>reglas= new ArrayList<>();
        reglas.add(RuleFactory.generateRule("Fecha Activacion"));
        return reglas;

    }

    private List<Regla> mediumConfig(){
        List<Regla>reglas= new ArrayList<>();
        reglas.add(RuleFactory.generateRule("Fecha Activacion"));
        reglas.add(RuleFactory.generateRule("Saldo Suficiente"));
        return reglas;
    }

    private List<Regla> fullConfig(){
        List<Regla>reglas= new ArrayList<>();
        reglas.add(RuleFactory.generateRule("Fecha Activacion"));
        reglas.add(RuleFactory.generateRule("Saldo Suficiente"));
        reglas.add(RuleFactory.generateRule("Monto Maximo"));
        return reglas;
    }
}