package co.com.validator;

public interface Regla {
    String execute (Transaccion transaccion);
}
