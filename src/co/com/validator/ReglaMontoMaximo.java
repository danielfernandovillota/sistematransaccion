package co.com.validator;

public class ReglaMontoMaximo implements Regla{

    @Override
    public String execute (Transaccion transaccion){
        if(transaccion.montoMovimiento>20000){
            return "Supera Monto Maximo";
        }
        return"";
    }
}
